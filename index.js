const express = require("express");
const connectToMongo = require("./db/database");
var cors = require("cors");

const app = express();
const port = process.env.PORT || 8000;

app.use(cors());
app.use(express.json());

// Available Routes
app.use("/api/auth", require("./controllers/auth"));
app.use("/api/adminController", require("./controllers/adminController"));

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});
