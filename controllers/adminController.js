const express = require("express");
const router = express.Router();
const { body, validationResult } = require("express-validator");
var database = require("../db/database");

var jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");

// we need to kept this JWT_SECRET in environment vairable but kept here because this website is only for demo.
const JWT_SECTRT = "employeekey";

router.post("/create_new_department", async (req, res) => {
  try {
    const data = req.body;
    const [rows_table] = await database.execute(`SHOW TABLES LIKE 'new_department'`);
    if (rows_table.length <= 0) {
      const [rows] = await database.query(`CREATE TABLE new_department (
              id INT PRIMARY KEY AUTO_INCREMENT,
              department_id VARCHAR(50) NOT NULL,
              department_name VARCHAR(50) NOT NULL,
              UNIQUE (department_id)
              )`);
    }
    let employee = await database.query(
      `SELECT * FROM new_department WHERE department_id='${req.body.department_id}'`
    );
    if (employee[0].length > 0) {
      return res.status(400).json({
        error: true,
        message: "Sorry a department with this number or model already exists",
      });
    }
    const [rows1] = await database.query(
      `INSERT INTO new_department (department_id, department_name) VALUES ('${data.department_id}', '${data.department_name}')`
    );
    res.status(200).json({
      error: false,
      message: "Department Data Insertded Successfully",
      data: rows1,
    });
  } catch (error) {
    console.error(error.message);
    res
      .status(500)
      .send({ error: true, message: "internal server Error occured" });
  }
});

router.post("/delete_employee", async (req, res) => {
  try {
    const data = req.body;

    const [rows1] = await database.query(
      `DELETE FROM employee WHERE id = ${data.id};`
    );
    res.status(200).json({
      error: false,
      message: "employee Data Deleted Successfully",
      data: rows1,
    });
  } catch (error) {
    console.error(error.message);
    res
      .status(500)
      .send({ error: true, message: "internal server Error occured" });
  }
});

router.post("/edit_employee", async (req, res) => {
  try {
    const data = req.body;

    const [rows] = await database.query(
      `SELECT * FROM employee
        WHERE id = ${data.id};`
    );
    let employeeDetail = rows[0];
    console.log(employeeDetail, "employeeDetail")
    const [rows1] = await database.query(
      `UPDATE employee
      SET 
      name =  '${
        data.name ? data.name : employeeDetail.name
      }',
      department = '${
        data.department ? data.department : employeeDetail.department
      }'
      WHERE id = ${data.id};`
    );
    res.status(200).json({
      error: false,
      message: "Employee Data Updated Successfully",
      data: rows1,
    });
  } catch (error) {
    console.error(error.message);
    res
      .status(500)
      .send({ error: true, message: "internal server Error occured" });
  }
});

router.post("/get_all_employee", async (req, res) => {
  try {
    let allemployee;
    console.log(req.body.search_value, "y");
    if (req.body.search_value) {
      allemployee = await database.query(`SELECT * FROM employee
       WHERE name LIKE '${req.body.search_value}%'`);
    } else {
      allemployee = await database.query(`SELECT * FROM employee`);
    }
    res.status(200).json({
      error: false,
      message: "employee Data Fetched Successfully",
      data: allemployee[0],
    });
  } catch (error) {
    console.error(error.message);
    res
      .status(500)
      .send({ error: true, message: "internal server Error occured" });
  }
});

router.post("/get_department", async (req, res) => {
  try {
    let allDepartment;
    allDepartment = await database.query(
        `SELECT * FROM new_department`
      );
    res.status(200).json({
      error: false,
      message: "employee Data Fetched Successfully",
      data: allDepartment[0],
    });
  } catch (error) {
    console.error(error.message);
    res
      .status(500)
      .send({ error: true, message: "internal server Error occured" });
  }
});

module.exports = router;
